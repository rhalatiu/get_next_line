/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rhalatiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/30 15:07:11 by rhalatiu          #+#    #+#             */
/*   Updated: 2018/01/06 16:54:47 by rhalatiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

void	ft_putnbr(int n)
{
	int		ct;
	int		a[15];
	long	nb2;

	nb2 = n;
	ct = 0;
	if (nb2 < 0)
	{
		ft_putchar('-');
		nb2 = nb2 * (-1);
	}
	while (nb2 > 9)
	{
		a[ct++] = nb2 % 10;
		nb2 = nb2 / 10;
	}
	a[ct] = nb2;
	while (ct >= 0)
	{
		ft_putchar(a[ct] + '0');
		ct--;
	}
}
